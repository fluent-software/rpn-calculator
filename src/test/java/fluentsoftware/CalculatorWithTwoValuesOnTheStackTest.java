package fluentsoftware;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyFloat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class CalculatorWithTwoValuesOnTheStackTest {

    private Calculator calculator;

    @BeforeEach
    void createCalculator() {
        calculator = new Calculator();
        calculator.push(5.0f);
        calculator.push(3.0f);
    }


    @Test
    void testInvokingAnOperationOnACalculatorWithTwoValuesOnTheStackResultsInStackWithSingleValue() {
        Operation operation = mock(Operation.class);
        calculator.invoke(operation);
        List<Float> stack = calculator.getStack();
        assertEquals(1, stack.size());
    }

    @Test
    void testInvokingAnOperationOnACalculatorWithTwoValuesOnTheStackResultsInACallToTheOperationCalculateWithTheValuesFromTheStack() {
        Operation operation = mock(Operation.class);
        calculator.invoke(operation);
        verify(operation).calculate(5.0f,3.0f);
    }

    @Test
    void testInvokingAnOperationOnACalculatorWithTwoValuesOnTheStackResultsInTheCalculatedValueOnTheStack() {
        Operation operation = mock(Operation.class);
        when(operation.calculate(anyFloat(),anyFloat())).thenReturn(2.0f);
        calculator.invoke(operation);
        List<Float> stack = calculator.getStack();
        assertEquals(2.0f, stack.get(0).floatValue());
    }
}
