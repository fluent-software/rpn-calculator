package fluentsoftware;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class AddOperationTest {
    @Test
    void testTheAddOperationPerformsTheCorrectCalculation() {
        Operation operation = new AddOperation();
        float result = operation.calculate(2.0f, 3.0f);
        assertEquals(5.0f, result);
    }
}
