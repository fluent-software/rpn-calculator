package fluentsoftware;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SubtractOperationTest {
    @Test
    void testTheSubtractOperationPerformsTheCorrectCalculation() {
        Operation operation = new SubtractOperation();
        float result = operation.calculate(8.0f, 2.0f);
        assertEquals(6.0f, result);
    }
}
