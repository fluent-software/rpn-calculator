package fluentsoftware;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.EmptyStackException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class CalculatorWithSingleValueOnTheStackTest {

    private Calculator calculator;

    @BeforeEach
    void createCalculator() {
        calculator = new Calculator();
        calculator.push(3.0f);
    }

    @Test
    void testPushingAValueOntoACalculatorWithItemsOnTheStackResultsInStackContainingBothValuesInOrder() {
        calculator.push(4.0f);
        List<Float> stack = calculator.getStack();
        assertEquals(3.0f, stack.get(0).floatValue());
        assertEquals(4.0f, stack.get(1).floatValue());
    }


    @Test
    void testInvokingAnOperationIsNotAllowedOnACalculatorWithASingleItemOnTheStack() {
        assertThrows(EmptyStackException.class, () -> {
            Operation operation = null;
            calculator.invoke(operation);
        });
    }

}
