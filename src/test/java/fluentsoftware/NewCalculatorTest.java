package fluentsoftware;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.EmptyStackException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class NewCalculatorTest {

    private Calculator calculator;

    @BeforeEach
    void createCalculator() {
        calculator = new Calculator();
    }

    @Test
    void testNewCalculatorHasEmptyStack() {
        List<Float> stack = calculator.getStack();
        assertTrue(stack.isEmpty());
    }

    @Test
    void testPushingAValueOntoANewCalculatorIncreasesTheStackSize() {
        calculator.push(1.0f);
        List<Float> stack = calculator.getStack();
        assertEquals(1, stack.size());
    }

    @Test
    void testPushingAValueOntoANewCalculatorResultsInPushedValueOnTheStack() {
        calculator.push(2.5f);
        List<Float> stack = calculator.getStack();
        assertEquals(2.5f, stack.get(0).floatValue());
    }

    @Test
    void testInvokingAnOperationIsNotAllowedOnANewCalculator() {
        assertThrows(EmptyStackException.class, () -> {
            Operation operation = null;
            calculator.invoke(operation);
        });
    }
}
