package fluentsoftware;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyFloat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CalculatorWithThreeValuesOnTheStackTest {

    private Calculator calculator;

    @BeforeEach
    void createCalculator() {
        calculator = new Calculator();
        calculator.push(2.0f);
        calculator.push(4.0f);
        calculator.push(6.0f);
    }


    @Test
    void testApplyingAnOperationToCalculatorWithThreeValuesResultsInFirstValuePlusCalculatedValueOnTheStack() {
        Operation operation = mock(Operation.class);
        when(operation.calculate(anyFloat(),anyFloat())).thenReturn(10.0f);
        calculator.invoke(operation);
        List<Float> stack = calculator.getStack();
        assertEquals(2.0f, stack.get(0).floatValue());
        assertEquals(10.0f, stack.get(1).floatValue());
    }
}
