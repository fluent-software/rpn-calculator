package fluentsoftware;

public class AddOperation implements Operation {
    @Override
    public float calculate(float v1, float v2) {
        return v1 + v2;
    }
}
