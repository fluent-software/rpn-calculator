package fluentsoftware;

public interface Operation {
    float calculate(float v1, float v2);
}
