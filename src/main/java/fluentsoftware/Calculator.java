package fluentsoftware;


import java.util.EmptyStackException;
import java.util.List;
import java.util.Stack;
import java.util.Vector;

public class Calculator {

    private Stack<Float> stack = new Stack<Float>();

    public List<Float> getStack() {
        return stack;
    }

    public void push(float v) {
        stack.add(v);
    }

    public void invoke(Operation operation) {
        Float v2 = stack.pop();
        Float v1 = stack.pop();
        Float result = operation.calculate(v1, v2);
        stack.push(result);
    }
}
